// IMPORT ORDER MODEL
const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');

// ORDER CREATION (NON-ADMIN/LOGGED-IN USER)
module.exports.createOrder = async (req, res) => {
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	};
	const owner = await User.findOne({_id: req.user.id}) 
	let newOrder = new Order({
		userId: owner.id,
		purchaser: `${owner.firstName} ${owner.lastName}`
	});
	newOrder.save()
	.then(order => res.send(order))
	.catch(err => res.send(err));
};

// RETRIEVE USER ORDERS
module.exports.userOrders = (req, res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.orders))
	.catch(err => res.send(err));
};

// RETRIEVE ALL ORDERS (ADMIN)
module.exports.getAllOrders = (req, res) => {
	Order.find({})
	.then(foundOrders => res.send(foundOrders))
	.catch(err => res.send(err));
};

// CREATE ORDER CREATION ALTERNATIVE CONTROLLER
/*
module.exports.createCart = async (req, res) => {
	const owner = req.user.id;
	const { productId, quantity } = req.body;
	try {
	    const order = await Order.findOne({ owner });
	    const product = await Product.findOne({ _id: productId });
	    const ownerDetails = await User.findOne({_id: req.user.id})
		if (!product) {
		    res.status(404).send({ message: "Product not found" });
		    return;
		}
		if(req.user.isAdmin){
			return res.send("Action Forbidden")
		};

		    const price = product.price;
		    const name = product.name;
		
		//no order exists, create one
		const newOrder = await Order.create({
		    owner,
		    products: [{ productId, name, quantity, price }],
		    userId: owner,
		    purchaser: ownerDetails.lastName,
		    orderDetails: [
		    	{
		    		productName: name,
		    		productId: product.id,
		    		productQuantity: quantity,
		    		subTotal: quantity * price
		    	}
		    ]
		})
	return res.status(201).send(newOrder);	
	} catch (error) {
	   console.log(error);
	   res.status(500).send("something went wrong");
	}
};
*/
// ADD PRODUCT TO EXISTING ORDER
/*module.exports.updateOrder = (req, res) => {
	const { productId, quantity } = req.body;
	const product = Product.findOne({ _id: productId });
	const owner = req.user.id;
	const foundOrder = Order.findOne(req.params.id)
	const price = product.price;
	const name = product.name;
	let updates = {
		Name: name,
	    Quantity: quantity,
	    totalAmount: quantity * price,
	    orderDetails: [
	    	{
	    		userId: owner,
	    		productId: productId
	    	}
	    ]
	};
	foundOrder.push(updates)
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};*/

// ADD TO ORDER TRIAL
/*
module.exports.addToOrder = async (req, res, next) => {
	let isOrderUpdated = await Order.findById(req.params.id).then(order => {
		let newOrder = {
			productId: req.body.productId
		};
		order.orderDetails.push(newOrder)
		return order.save()
		.then(order => true)
		.catch(err => err.message);
	})
};
*/