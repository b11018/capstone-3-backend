// DEPENDENCIES
const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// USER REGISTRATION
module.exports.registerUser = (req, res) => {
	// console.log(req.body);
	
		
	const hashedPW = bcrypt.hashSync(req.body.password, 10);
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPW
	});
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
		
}	
	


// USER LOGIN
module.exports.loginUser = (req, res) => {
	console.log(req.body);
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("No user found in the database");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);
			console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send("Incorrect password, please try again");
			}
		} 
	})
	.catch(err => res.send(err));
};

// UPDATE TO ADMIN
module.exports.updateAdmin = (req, res) => {
	let updates = {
		isAdmin: true
	};
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// USER CHECKOUT (NON-ADMIN)
module.exports.userCheckout = async (req, res) => {
	// Checks if Admin
	if(req.user.isAdmin){
		return res.send(false)
	};
	// Declaring Variables
	const owner = await User.findOne({ _id: req.user.id });
	const item = await Product.findOne({ _id: req.body.productId });
	const quantity = req.body.quantity
	// Searches for user and updates its 'orders' subdoc
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newPurchase = {
			purchaser: `${owner.firstName} ${owner.lastName}`,
			productId: item._id,
			productName: item.name,
			price: item.price,
			quantity: quantity,
			subTotal: item.price * quantity
		};
		user.orders.push(newPurchase);
		return user.save()
		.then(user => true)
		.catch(err => err.message);
	});
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	};
	// Searches for product and updates its 'consumers' subdoc
	let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		let consumer = {
			purchaser: `${owner.firstName} ${owner.lastName}`,
			productId: item._id,
			productName: item.name,
			price: item.price,
			quantity: req.body.quantity,
			subTotal: item.price * quantity
		};
		product.consumers.push(consumer)
		return product.save()
		.then(product => true)
		.catch(err => err.message);
	});
	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	};
	// Checks if all models are updated
	if(isUserUpdated && isProductUpdated){
		return res.send({message: 'User Purchase successful'})
	};
};

// [CAPSTONE 3] GETTING LOGGED IN USER DETAILS
module.exports.getUserDetails = (req, res) => {

	// console.log(req.user);

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [CAPSTONE 3] GETTING CUSTOMER DETAILS
module.exports.getCustomerDetails = (req, res) => {
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [CAPSTONE 3] CHECK IF EMAIL EXISTS
module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}

// [CAPSTONE 3] DELETE ITEM FROM CART using productId
module.exports.deleteOrder = async (req, res) => {
	
	const owner = await User.findById(req.user.id);
	let orderIndex = await owner.orders.findIndex(order => order.productId === req.body.productId);
	owner.orders.splice(orderIndex, 1)
	console.log(orderIndex)
	console.log(owner.orders)
	return owner.save()
		.then(owner => true)
		// .then(res.send({message: 'Splice and save successful'}))
		.catch(err => err.message);
		
	// let orderIndex = await owner.orders.findIndex(order => order.productId == req.body.productId)
	// owner.orders.splice(orderIndex, 1)
	// return orders.save()
	// .then(orders => true)
	// .catch(err => err.message);

	// User.orders.findIndex(order => order._id == req.params.id)
	// .then(result => User.orders.splice(result, 1))
	// .catch(err => res.send(err));
	// console.log(orderIndex)

	// let consumerIndex = Product.consumers.findIndex(consumer => consumer._id === id);
	// Product.consumers.splice(consumerIndex, 1);
	// return res.send(true);

};

// CAPSTONE 3 GET ALL NON-ADMIN USERS FOR SHOW USER ORDERS
module.exports.getAllUsers = (req, res) => {
	User.find({isAdmin: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};