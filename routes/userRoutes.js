// DEPENDENCIES
const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// USER REGISTRATION ROUTE
router.post('/userRegistration', userControllers.registerUser);

// USER LOGIN ROUTE
router.post('/login', userControllers.loginUser);

// UPDATE TO ADMIN ROUTE
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

// USER CHECKOUT ROUTE
router.post('/userCheckout', verify, userControllers.userCheckout);

// [CAPSTONE 3] GETTING LOGGED IN USER DETAILS
router.post('/getUserDetails', verify, userControllers.getUserDetails)

// [CAPSTONE 3] GETTING CUSTOMER DETAILS
router.post('/getCustomerDetails/:id', verify, verifyAdmin, userControllers.getCustomerDetails)

// Check if Email Exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

// [CAPSTONE 3] DELETE ITEM FROM CART using productId
router.put('/deleteOrder', verify, userControllers.deleteOrder);

// CAPSTONE 3 GET ALL NON-ADMIN USERS FOR SHOW USER ORDERS
router.post('/getAllUsers', verify, verifyAdmin, userControllers.getAllUsers);

module.exports = router;