// DEPENDENCIES
const mongoose = require('mongoose');
// PRODUCT SCHEMA
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, 'Product name required']
	},
	description: {
		type: String,
		require: [true, 'Product description required']
	},
	price: {
		type: Number,
		require: [true, 'Product price required']
	},
	inStock: {
		type: Boolean,
		default: true
	},
	dateFirstAvailable: {
		type: Date,
		default: new Date()
	},
	consumers: [
		{
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			purchaser: {
				type: String,
				required: [true, "Purchaser is required"]
			},
			productId: {
				type: String,
				required: [true, "Product ID required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name required"]
			},
			price: {
				type: Number,
				require: [true, 'Product price required']
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Subtotal is required"]
			}
		}
	]
});
module.exports = mongoose.model("Product", productSchema);