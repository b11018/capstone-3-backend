// DEPENDENCIES
const mongoose = require('mongoose');
// ORDER SCHEMA
const orderSchema = new mongoose.Schema([
	{
		userId: {
			type: String,
			required: [true, "User Id is required"]
		},
		purchaser: {
			type: String,
			required: [true, "Purchaser is required"]
		},
		orderDetails: [
			{
				purchasedOn: {
					type: Date,
					default: new Date()
				},
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				productName: {
					type: String,
					required: [true, "Product Name is required"]
				},
				productQuantity: {
					type: Number,
					required: [true, "Quantity is required"]
				},
				subTotal: {
					type: Number,
					required: [true, "Subtotal is required"]
				}
			}
		]
	}
]);

module.exports = mongoose.model("Order", orderSchema);