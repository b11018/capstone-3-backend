// DEPENDENCIES
const mongoose = require('mongoose');
// USER SCHEMA
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number required"]
	},
	email: {
		type: String,
		required: [true, "Email required"]
	},
	password: {
		type: String,
		required: [true, "Password required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			datePurchased: {
				type: Date,
				default: new Date()
			},
			purchaser: {
				type: String,
				required: [true, "Purchaser is required"]
			},
			productId: {
				type: String,
				required: [true, "Product ID required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name required"]
			},
			price: {
				type: Number,
				require: [true, 'Product price required']
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Subtotal is required"]
			}
		}
	]
});
module.exports = mongoose.model("User", userSchema);